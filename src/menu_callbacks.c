#include <glib.h>
#include <gtk/gtk.h>

#include <libintl.h>
#include <locale.h>

#define  _(x)     gettext (x)
#define N_(x)     x
#define C_(ctx,x) pgettext (ctx, x)

#include "menu_callbacks.h"
#include "proscenion_app.h"
#include "proscenion_app_win.h"

void
new_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data)
{
	ProscenionApp* proscenion = PROSCENION_APP(user_data);
	GtkWindow *window = gtk_application_get_active_window(GTK_APPLICATION(proscenion));
	ProscenionAppWindow *proscenion_window = PROSCENION_APP_WINDOW(window);

	GtkWidget *dialog_window;
	GtkWidget *content_area;
	GtkWidget *width_spin, *height_spin, *label;
	GtkAdjustment *adjustment_width, *adjustment_height;
	
	gdouble w, h;
	proscenion_app_window_get_dimensions(proscenion_window, &w, &h);

	adjustment_width = gtk_adjustment_new (w, 1.0, 50.0, 1.0, 5.0, 0.0);
	adjustment_height = gtk_adjustment_new (h, 1.0, 50.0, 1.0, 5.0, 0.0);
	label = gtk_label_new (_("width x height (meters)"));
	width_spin = gtk_spin_button_new(adjustment_width, 1.0, 0);
	height_spin = gtk_spin_button_new(adjustment_height, 1.0, 0);

	dialog_window = gtk_dialog_new_with_buttons ( _("New Stage"), window, GTK_DIALOG_DESTROY_WITH_PARENT, _("_OK"), GTK_RESPONSE_ACCEPT, _("_Cancel"), GTK_RESPONSE_REJECT, NULL);
	content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog_window));
	gtk_container_add (GTK_CONTAINER (content_area), label);
	gtk_container_add (GTK_CONTAINER (content_area), width_spin);
	gtk_container_add (GTK_CONTAINER (content_area), height_spin);
	gtk_widget_show(label);
	gtk_widget_show(width_spin);
	gtk_widget_show(height_spin);
	int result = gtk_dialog_run(GTK_DIALOG (dialog_window));
	int width, height;
	switch (result) {
		case GTK_RESPONSE_ACCEPT:
			width = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(width_spin));
			height = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(height_spin));
			proscenion_app_window_set_dimensions(proscenion_window, width, height);
			break;
		case GTK_RESPONSE_REJECT:
			break;
	}

	gtk_widget_destroy(dialog_window);
}

void
open_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data)
{
	ProscenionApp* proscenion = PROSCENION_APP(user_data);
	GtkWindow *window = gtk_application_get_active_window(GTK_APPLICATION(proscenion));
	ProscenionAppWindow *proscenion_window = PROSCENION_APP_WINDOW(window);
	GtkWidget *dialog;
	gint res;

	dialog = gtk_file_chooser_dialog_new (_("Open File"),
			window,
			GTK_FILE_CHOOSER_ACTION_OPEN,
			_("_Cancel"),
			GTK_RESPONSE_CANCEL,
			_("_Open"),
			GTK_RESPONSE_ACCEPT,
			NULL);

	GtkFileFilter *filter = gtk_file_filter_new();
	gtk_file_filter_add_pattern (filter, PROSCENION_FILE_PATTERN);
	gtk_file_filter_set_name (filter, _("Proscenion stage files"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(dialog), filter);
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT)
	{
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		filename = gtk_file_chooser_get_filename (chooser);
		GFile* file = g_file_new_for_path (filename);
		proscenion_app_window_open (proscenion_window, file);
		g_free (filename);
		g_object_unref(file);
	}

	gtk_widget_destroy (dialog);
}

void
save_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data)
{
  ProscenionApp* proscenion = PROSCENION_APP(user_data);
	GtkWindow *window = gtk_application_get_active_window(GTK_APPLICATION(proscenion));
	ProscenionAppWindow *proscenion_window = PROSCENION_APP_WINDOW(window);
	GtkWidget *dialog;
	gint res;

	dialog = gtk_file_chooser_dialog_new (_("Open File"),
			window,
      GTK_FILE_CHOOSER_ACTION_SAVE,
      _("_Cancel"),
			GTK_RESPONSE_CANCEL,
      _("_Save"),
      GTK_RESPONSE_ACCEPT,
      NULL);
  GtkFileFilter *filter = gtk_file_filter_new();
	gtk_file_filter_add_pattern (filter, PROSCENION_FILE_PATTERN);
	gtk_file_filter_set_name (filter, _("Proscenion stage files"));
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(dialog), filter);
	res = gtk_dialog_run (GTK_DIALOG (dialog));
  if (res == GTK_RESPONSE_ACCEPT)
	{
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		filename = gtk_file_chooser_get_filename (chooser);
		GFile* file = g_file_new_for_path (filename);
    proscenion_app_window_save(proscenion_window, file);
    g_free (filename);
		g_object_unref(file);
	}

	gtk_widget_destroy (dialog);
}

void
addsrc_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data)
{
	static int idx = 0;
	ProscenionApp* proscenion = PROSCENION_APP(user_data);
	GtkWindow *window = gtk_application_get_active_window(GTK_APPLICATION(proscenion));
	ProscenionAppWindow *proscenion_window = PROSCENION_APP_WINDOW(window);

	GtkWidget *dialog_window;
	GtkWidget *content_area;
	GtkWidget *width_spin, *height_spin, *label1, *label2, *input;
	GtkAdjustment *adjustment_width, *adjustment_height;

	gdouble w, h;
	proscenion_app_window_get_dimensions(proscenion_window, &w, &h);

	adjustment_width = gtk_adjustment_new (0.0, -w / 2, w / 2, 1.0, 5.0, 0.0);
	adjustment_height = gtk_adjustment_new (1.0,  - h / 2, h / 2, 1.0, 5.0, 0.0);
	label1 = gtk_label_new (_("x, y (meters)"));
	label2 = gtk_label_new (_("name"));
	input = gtk_entry_new();
	gtk_entry_set_placeholder_text(GTK_ENTRY(input), "instrument");
	width_spin = gtk_spin_button_new(adjustment_width, 1.0, 0);
	height_spin = gtk_spin_button_new(adjustment_height, 1.0, 0);

	dialog_window = gtk_dialog_new_with_buttons ( _("New Source"), window, GTK_DIALOG_DESTROY_WITH_PARENT, _("_OK"), GTK_RESPONSE_ACCEPT, _("_Cancel"), GTK_RESPONSE_REJECT, NULL);
	content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog_window));
	gtk_container_add (GTK_CONTAINER (content_area), label1);
	gtk_container_add (GTK_CONTAINER (content_area), width_spin);
	gtk_container_add (GTK_CONTAINER (content_area), height_spin);
	gtk_container_add (GTK_CONTAINER (content_area), label2);
	gtk_container_add (GTK_CONTAINER (content_area), input);

	gtk_widget_show(label1);
	gtk_widget_show(width_spin);
	gtk_widget_show(height_spin);
	gtk_widget_show(label2);
	gtk_widget_show(input);

	int result = gtk_dialog_run(GTK_DIALOG (dialog_window));
	double x, y;
	const gchar *name;
	switch (result) {
		case GTK_RESPONSE_ACCEPT:
			x = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(width_spin));
			y = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON(height_spin));
			name = 	gtk_entry_get_text(GTK_ENTRY(input));
			if (!name || name[0] == '\0')
				name = g_strdup_printf(_("instrument %d"), ++idx);

			if (!proscenion_app_window_add_item(proscenion_window, name, x, y, 1.0, FALSE))
				g_warning("Source %s already existing. Not added.", name);
			break;
		case GTK_RESPONSE_REJECT:
			break;
	}

	gtk_widget_destroy(dialog_window);

}

static void
on_dialog_close (GtkDialog *dialog,
          gint       response_id,
          gpointer   user_data)
{
  gtk_widget_destroy (GTK_WIDGET (dialog));
}

void
about_callback (GSimpleAction *simple,
            GVariant      *parameter,
            gpointer       user_data)
{
   GtkWidget *about_dialog;

   about_dialog = gtk_about_dialog_new ();

   const gchar *authors[] = {"Benoît Rouits", NULL};
   const gchar *documenters[] = {"Benoît Rouits", NULL};

   /* Fill in the about_dialog with the desired information */
   gtk_about_dialog_set_logo_icon_name (GTK_ABOUT_DIALOG (about_dialog), "proscenion");
   gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (about_dialog), "Proscenion");
   gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (about_dialog), "Copyright \xc2\xa9 2018 Benoît Rouits");
   gtk_about_dialog_set_license_type(GTK_ABOUT_DIALOG (about_dialog), GTK_LICENSE_GPL_3_0);
   gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (about_dialog), authors);
   gtk_about_dialog_set_documenters (GTK_ABOUT_DIALOG (about_dialog), documenters);
   gtk_about_dialog_set_website_label (GTK_ABOUT_DIALOG (about_dialog), "Proscenion Website");
   gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (about_dialog), "http://brouits.free.fr/proscenion");

   gtk_window_set_title (GTK_WINDOW (about_dialog), "");

   g_signal_connect (GTK_DIALOG (about_dialog), "response",
                     G_CALLBACK (on_dialog_close), NULL);

   gtk_widget_show (about_dialog);
}
