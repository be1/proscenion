/* main.c
 *
 * Copyright 2018 Benoît Rouits
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <gtk/gtk.h>

#include <libintl.h>
#include <locale.h>

#define  _(x)     gettext (x)
#define N_(x)     x
#define C_(ctx,x) pgettext (ctx, x)

#include "proscenion_app.h"
#include "menu_callbacks.h"

#include "config.h"

int main(int   argc,
         char *argv[])
{
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALEDIR);
  bind_textdomain_codeset (PACKAGE_TARNAME, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  return g_application_run (G_APPLICATION (proscenion_app_new ()), argc, argv);
}
