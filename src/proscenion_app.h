#ifndef __PROSCENION_APP_H
#define __PROSCENION_APP_H

#include <glib.h>
#include <gtk/gtk.h>

#define PROSCENION_STAGE_DEFAULT_WIDTH 6.4
#define PROSCENION_WINDOW_WIDTH 640.0

#define PROSCENION_STAGE_DEFAULT_HEIGHT 4.8
#define PROSCENION_WINDOW_HEIGHT 480.0

#define PROSCENION_PIX_PER_METER (PROSCENION_WINDOW_WIDTH / PROSCENION_STAGE_DEFAULT_WIDTH)

#define PROSCENION_APP_TYPE (proscenion_app_get_type ())
G_DECLARE_FINAL_TYPE (ProscenionApp, proscenion_app, PROSCENION, APP, GtkApplication)


ProscenionApp     *proscenion_app_new         (void);

#endif
