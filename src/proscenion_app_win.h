#ifndef __PROSCENIONAPPWIN_H
#define __PROSCENIONAPPWIN_H

#include <glib.h>
#include <gtk/gtk.h>
#include <libxml/tree.h>
#include "proscenion_app.h"


#define PROSCENION_APP_WINDOW_TYPE (proscenion_app_window_get_type ())
G_DECLARE_FINAL_TYPE (ProscenionAppWindow, proscenion_app_window, PROSCENION, APP_WINDOW, GtkApplicationWindow)


ProscenionAppWindow       *proscenion_app_window_new          (ProscenionApp *app);
void                    proscenion_app_window_open         (ProscenionAppWindow *win,
                                                         GFile            *file);
void                    proscenion_app_window_get_dimensions  (ProscenionAppWindow *win,
                                                         gdouble *width, gdouble *height);
void                    proscenion_app_window_set_dimensions  (ProscenionAppWindow *win,
                                                         gdouble width, gdouble height);
gboolean                proscenion_app_window_add_item  (ProscenionAppWindow *win, const gchar *name,
                                                         gdouble x, gdouble y, gdouble sens, gboolean capture);
gboolean                proscenion_app_window_remove_item  (ProscenionAppWindow *win, const gchar *name, gboolean capture);

void                    proscenion_app_window_configure (ProscenionAppWindow* win, xmlNodePtr root);

void                    proscenion_app_window_display (ProscenionAppWindow* win, xmlNodePtr item);

void                    proscenion_app_window_save(ProscenionAppWindow* win, GFile* file);

#endif

