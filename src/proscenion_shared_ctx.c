#include "proscenion_shared_ctx.h"

ProscenionSharedCtx *proscenion_shared_ctx_new () {
	ProscenionSharedCtx *ctx = g_new(ProscenionSharedCtx, 1);
	g_mutex_init(&ctx->m);
	ctx->items = NULL;
	ctx->capture = NULL;
	return ctx;
}
