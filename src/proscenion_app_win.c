#include <locale.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <goocanvas.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include <libintl.h>
#include <locale.h>

#define  _(x)     gettext (x)
#define N_(x)     x
#define C_(ctx,x) pgettext (ctx, x)

#include "proscenion_app.h"
#include "proscenion_app_win.h"
#include "proscenion_item.h"
#include "proscenion_shared_ctx.h"

struct _ProscenionAppWindow
{
  GtkApplicationWindow parent;
};

typedef struct _ProscenionAppWindowPrivate ProscenionAppWindowPrivate;

struct _ProscenionAppWindowPrivate
{
  GtkWidget *canvas;
  GtkWidget *frame;
  GtkWidget *info;
  GtkWidget *sens;
  ProscenionSharedCtx* ctx;
  ProscenionItem* selection;
};

G_DEFINE_TYPE_WITH_PRIVATE(ProscenionAppWindow, proscenion_app_window, GTK_TYPE_APPLICATION_WINDOW);

typedef struct _ProscenionContextMenuData ProscenionContextMenuData;

struct _ProscenionContextMenuData {
  ProscenionAppWindow *win;
  GooCanvasItem *item;
};

  static void
proscenion_app_window_init (ProscenionAppWindow *win)
{
  gtk_window_set_title (GTK_WINDOW (win), "Proscenion");
  gtk_window_set_default_size (GTK_WINDOW (win), 320, 200);
}

  static void
proscenion_app_window_class_init (ProscenionAppWindowClass *class)
{
}

  static gboolean
on_item_motion (GooCanvasItem  *item,
    GooCanvasItem  *target_item,
    GdkEventMotion *event,
    gpointer        user_data)
{
  ProscenionAppWindowPrivate *priv;
  ProscenionAppWindow *win = (ProscenionAppWindow*) user_data;
  priv = proscenion_app_window_get_instance_private (win);
  ProscenionSharedCtx *ctx = priv->ctx;

  GList* it;
  for (it = ctx->items; it; it = it->next) {
    ProscenionItem *pitem = (ProscenionItem*)(it->data);
    if (pitem->item == item && pitem->grabbed == TRUE) {
      gdouble rx;
      gdouble ry;
      rx = event->x; ry = event->y;
      GooCanvas *canvas = goo_canvas_item_get_canvas (item);
      goo_canvas_convert_from_pixels (canvas, &rx, &ry);
      g_object_set(item, "x", rx, NULL);
      g_object_set(item, "y", ry, NULL);
      gdouble left, top, right, bottom;
      goo_canvas_get_bounds (canvas, &left, &top, &right, &bottom);
      pitem->x = (rx - (right - left) / 2) / PROSCENION_PIX_PER_METER;
      pitem->y = (ry - (bottom - top) / 2) / PROSCENION_PIX_PER_METER;
      gchar *info = g_strdup_printf("%.02f x %.02f", pitem->x, pitem->y);
      gtk_label_set_text(GTK_LABEL(priv->info), info);
      g_free(info);
      break;
    }
  }
  return TRUE;
}

  void
on_context_menu_item_remove(GtkMenuItem *menu_item, gpointer user_data)
{
  ProscenionContextMenuData *data = (ProscenionContextMenuData*) user_data;
  ProscenionAppWindowPrivate *priv = proscenion_app_window_get_instance_private (data->win);
  ProscenionSharedCtx *ctx = priv->ctx;
  GList *it;
  for (it = ctx->items; it; it = it->next) {
    ProscenionItem *pitem = it->data;
    if (pitem->item == data->item) {
      proscenion_app_window_remove_item (data->win, pitem->name, pitem->capture);
    }
  }
  free(data);
}

  static gboolean
on_item_button_press (GooCanvasItem  *item,
    GooCanvasItem  *target,
    GdkEventButton *event,
    gpointer        user_data)
{
  ProscenionAppWindowPrivate *priv;
  ProscenionAppWindow *win = (ProscenionAppWindow*) user_data;
  priv = proscenion_app_window_get_instance_private (win);
  ProscenionSharedCtx *ctx = priv->ctx;

  if (event->button != 1 && event->button != 3)
    return FALSE;

  if (event->button == 1) {
    /* left button */
    GList *it;
    switch (event->type) {
      case GDK_BUTTON_PRESS:
        for (it = ctx->items; it; it = it->next) {
          ProscenionItem *pitem = it->data;
          if (pitem->item == item) {
            pitem->grabbed = TRUE;
            priv->selection = pitem;
            gtk_frame_set_label (GTK_FRAME(priv->frame), pitem->name);
            gchar *info = g_strdup_printf("%.02f x %.02f", pitem->x, pitem->y);
            gtk_label_set_text (GTK_LABEL(priv->info), info);
            g_free(info);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON(priv->sens), pitem->sens);
          } else {
            pitem->grabbed = FALSE;
          }
        }
        break;
      case GDK_BUTTON_RELEASE:
        for (it = ctx->items; it; it = it->next) {
          ProscenionItem *pitem = (ProscenionItem*)(it->data);
          if (pitem->item == item) {
            pitem->grabbed = FALSE;
            break;
          }
        }
        break;
      default:
        g_print("default\n");
    }
  } else {
    /* right button */
    GList *it;
    for (it = ctx->items; it; it = it->next) {
      ProscenionItem *pitem = (ProscenionItem*)(it->data);
      if (pitem->item == item) {
        if (pitem->capture) {
          /* never show context menu on capture */
          return FALSE;
        } else {
          GtkWidget *menu;
          menu = gtk_menu_new ();
          GtkWidget *menu_item;
          menu_item = gtk_menu_item_new_with_label (_("remove"));
          gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
          ProscenionContextMenuData *menu_data = calloc(1, sizeof(ProscenionContextMenuData));
          menu_data->win = win;
          menu_data->item = item;
          g_signal_connect (G_OBJECT(menu_item), "activate", G_CALLBACK(on_context_menu_item_remove), menu_data);
          gtk_widget_show(menu_item);
          gtk_menu_popup_at_pointer(GTK_MENU(menu), (GdkEvent*) event);
          break;
        }
      }
    }
  }
  return TRUE;
}

  gboolean
proscenion_app_window_remove_item  (ProscenionAppWindow *win, const gchar *name, gboolean capture)
{
  ProscenionAppWindowPrivate *priv;
  priv = proscenion_app_window_get_instance_private (win);
  ProscenionSharedCtx *ctx = priv->ctx;
  g_mutex_lock(&ctx->m);

  GList* it;
  for (it = ctx->items; it; it = it->next) {
    ProscenionItem *pitem = it->data;
    if (!strcmp(name, pitem->name) && pitem->capture == capture) {
      g_info ("unregistering jack ports for %s\n", name);
      jack_port_unregister(ctx->jack->client, pitem->portL);
      jack_port_unregister(ctx->jack->client, pitem->portR);
      g_free(pitem->name);
      goo_canvas_item_remove(pitem->item);
      pitem->item = NULL;
      ctx->items = g_list_remove(ctx->items, it->data);
      if (capture) {
        ctx->capture = NULL;
      }
      g_mutex_unlock(&ctx->m);
      return TRUE;
    }
  }

  g_mutex_unlock(&ctx->m);
  return FALSE;
}

static void sens_changed (GtkSpinButton *spin, gpointer user_data)
{
  ProscenionAppWindowPrivate *priv = user_data;
  ProscenionItem *pitem = priv->selection;
  pitem->sens = gtk_spin_button_get_value (spin);
}

  ProscenionAppWindow *
proscenion_app_window_new (ProscenionApp *app)
{
  ProscenionAppWindowPrivate *priv;
  GObject *obj = g_object_new (PROSCENION_APP_WINDOW_TYPE, "application", app, NULL);

  ProscenionAppWindow *win = PROSCENION_APP_WINDOW(obj);
  priv = proscenion_app_window_get_instance_private (win);
  priv->ctx = proscenion_shared_ctx_new();
  priv->selection = NULL;

  /* initiate jack stuff */
  priv->ctx->jack = psc_jack_new ();
  jack_set_process_callback (priv->ctx->jack->client, psc_jack_process, priv->ctx);
  jack_on_shutdown (priv->ctx->jack->client, psc_jack_shutdown, priv->ctx);
  psc_jack_start(priv->ctx->jack);

  gtk_window_set_title (GTK_WINDOW (win), "Proscenion");
  gtk_window_set_default_size (GTK_WINDOW (win), PROSCENION_WINDOW_WIDTH, PROSCENION_WINDOW_HEIGHT);

  GtkWidget *hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_container_add(GTK_CONTAINER(win), hbox);
  gtk_widget_show(hbox);

  GtkWidget *scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW(scrolled), GTK_SHADOW_IN);
  gtk_box_pack_start (GTK_BOX(hbox), scrolled, TRUE, TRUE, 0);
  gtk_widget_show(scrolled);

  GtkWidget *vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_box_pack_start (GTK_BOX(hbox), vbox, FALSE, TRUE, 0);
  gtk_widget_show(vbox);

  priv->frame = gtk_frame_new(_("item"));
  gtk_box_pack_start(GTK_BOX(vbox), priv->frame, FALSE, TRUE, 0);
  gtk_widget_show(priv->frame);

  priv->info = gtk_label_new("___ x ___");
  gtk_container_add(GTK_CONTAINER(priv->frame), priv->info);
  gtk_widget_show(priv->info);

  GtkWidget *frame = gtk_frame_new(_("sensitivity"));
  gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, TRUE, 0);
  gtk_widget_show(frame);

  GtkAdjustment *adjustment_sens;
  adjustment_sens = gtk_adjustment_new (1.0, 0.1, 10.0, 0.1, 1.0, 0.0);
  priv->sens = gtk_spin_button_new (adjustment_sens, 1.0, 3);
  gtk_container_add(GTK_CONTAINER(frame), priv->sens);
  gtk_widget_show(priv->sens);
  g_signal_connect (priv->sens, "value_changed", G_CALLBACK (sens_changed), priv);

  priv->canvas = goo_canvas_new ();
  goo_canvas_set_bounds (GOO_CANVAS(priv->canvas), 0, 0, PROSCENION_WINDOW_WIDTH, PROSCENION_WINDOW_HEIGHT);

  gtk_widget_set_events (GTK_WIDGET(priv->canvas), gtk_widget_get_events (GTK_WIDGET(priv->canvas))
      | GDK_BUTTON_PRESS_MASK
      | GDK_BUTTON_RELEASE_MASK
      | GDK_POINTER_MOTION_MASK);
  GValue bgcolor = G_VALUE_INIT;
  g_value_init (&bgcolor, G_TYPE_STRING);
  g_value_set_static_string(&bgcolor, "white");
  g_object_set_property (G_OBJECT(priv->canvas), "background-color", &bgcolor);
  /*
     GValue center = G_VALUE_INIT;
     g_value_init(&center, G_TYPE_INT);
     g_value_set_int(&center, 0);
     g_object_set_property (G_OBJECT(priv->canvas), "anchor", &center);
     */
  proscenion_app_window_add_item(win, "capture", 0, 0, 1.0, TRUE);
  gtk_container_add (GTK_CONTAINER (scrolled), priv->canvas);
  gtk_widget_show(priv->canvas);

  return win;
}

  void
proscenion_app_window_save(ProscenionAppWindow* win, GFile* file)
{
  ProscenionAppWindowPrivate *priv = proscenion_app_window_get_instance_private (win);
  ProscenionSharedCtx *ctx = priv->ctx;
  ProscenionItem *capture = ctx->capture;
  GList *list = ctx->items;
  GList* it;
  locale_t prev = uselocale ((locale_t)0);
  locale_t loc = newlocale (LC_NUMERIC, "C", (locale_t)0);
  uselocale (loc);

  char* path = g_file_get_path(file);
  FILE *stream = fopen(path, "w");
  fprintf(stream, "<proscenion><stage>");

  for (it = list; it; it = it->next) {
    ProscenionItem * item = it->data;
    if (item->capture)
      continue;
    fprintf(stream, "<item name=\"%s\" x=\"%f\" y=\"%f\" sens=\"%f\" />", item->name, item->x, item->y, item->sens);
  }
  fprintf(stream, "</stage>");
  fprintf(stream, "<capture x=\"%f\" y=\"%f\" sens=\"%f\" />", capture->x, capture->y, capture->sens);
  fprintf(stream, "</proscenion>");
  fclose(stream);

  loc = uselocale (prev);
  freelocale (loc);
}

  void
proscenion_app_window_display(ProscenionAppWindow* win, xmlNodePtr item)
{
  gboolean cap = FALSE;
  xmlChar *name, *x, *y, *sens;
  name = xmlGetProp (item, "name");
  x = xmlGetProp (item, "x");
  y = xmlGetProp (item, "y");
  sens = xmlGetProp (item, "sens");

  if (!g_strcmp0(item->name, "capture")) {
    name = g_strdup(item->name);
    cap = TRUE;
  }

  proscenion_app_window_add_item (win, name, atof(x), atof(y), atof(sens), cap);

  xmlFree (name);
  xmlFree (x);
  xmlFree (y);
  xmlFree (sens);
}

void browse_rec(ProscenionAppWindow* win, xmlNodePtr root)
{
  xmlNodePtr cur_node = NULL;

  for (cur_node = root; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      if (cur_node && !g_strcmp0(cur_node->name, "item"))
        proscenion_app_window_display(win, cur_node);
      if (cur_node && !g_strcmp0(cur_node->name, "capture"))
        proscenion_app_window_display(win, cur_node);
    }
    browse_rec(win, cur_node->children);
  }
}

  void
proscenion_app_window_configure (ProscenionAppWindow* win, xmlNodePtr root)
{
  locale_t prev = uselocale ((locale_t)0);
  locale_t loc = newlocale (LC_NUMERIC, "C", (locale_t)0);

  uselocale (loc);
  browse_rec(win, root);
  loc = uselocale (prev);
  freelocale (loc);
}

  void
proscenion_app_window_clear (ProscenionAppWindow *win)
{
  ProscenionAppWindowPrivate* priv;
  priv = proscenion_app_window_get_instance_private (win);
  ProscenionSharedCtx* ctx = priv->ctx;

  GList* copy = g_list_copy (ctx->items);
  GList* it;
  for (it = copy; it; it = it->next) {
    ProscenionItem *pitem = it->data;
    proscenion_app_window_remove_item (win, pitem->name, pitem->capture);
  }
  g_list_free(copy);
}

  void
proscenion_app_window_open (ProscenionAppWindow *win,
    GFile            *file)
{
  ProscenionAppWindowPrivate *priv;
  gchar *basename;
  GtkWidget *scrolled, *view;
  gchar *contents;
  gsize length;

  LIBXML_TEST_VERSION

    priv = proscenion_app_window_get_instance_private (win);
  basename = g_file_get_basename (file);

  proscenion_app_window_clear (win);
  gtk_window_set_title (GTK_WINDOW (win), basename);
  if (g_file_load_contents (file, NULL, &contents, &length, NULL, NULL))
  {
    xmlDocPtr doc;
    doc = xmlReadMemory (contents, length, basename, NULL, 0);
    if (doc == NULL) {
      fprintf(stderr, "Failed to parse document\n");
      return;
    }

    xmlNodePtr root_element = NULL;
    root_element = xmlDocGetRootElement (doc);
    proscenion_app_window_configure (win, root_element);
    xmlFreeDoc (doc);
    g_free (contents);
    xmlCleanupParser ();
  }

  g_free (basename);
}

  void
proscenion_app_window_get_dimensions (ProscenionAppWindow *win,
    gdouble *width, gdouble *height)
{
  ProscenionAppWindowPrivate *priv;
  priv = proscenion_app_window_get_instance_private (win);

  gdouble left, top, right, bottom;

  goo_canvas_get_bounds(GOO_CANVAS(priv->canvas), &left, &top, &right, &bottom);
  if (width)
    *width = (right - left) / PROSCENION_PIX_PER_METER;
  if (height)
    *height = (bottom - top) / PROSCENION_PIX_PER_METER;
}

  void
proscenion_app_window_set_dimensions (ProscenionAppWindow *win,
    gdouble width, gdouble height)
{
  ProscenionAppWindowPrivate *priv;
  priv = proscenion_app_window_get_instance_private (win);
  goo_canvas_set_bounds (GOO_CANVAS(priv->canvas), 0, 0, PROSCENION_PIX_PER_METER * width, PROSCENION_PIX_PER_METER * height);
}

  gboolean
proscenion_app_window_add_item  (ProscenionAppWindow *win, const gchar *name,
    double x, gdouble y, gdouble sens, gboolean capture)
{
  ProscenionAppWindowPrivate *priv;
  priv = proscenion_app_window_get_instance_private (win);

  GList* it;
  ProscenionSharedCtx* ctx = priv->ctx;
  g_mutex_lock(&ctx->m);
  for (it = ctx->items; it; it = it->next) {
    ProscenionItem *pitem = it->data;
    /* check possible existence */
    if (!g_strcmp0(name, pitem->name) && pitem->capture == capture) {
      g_warning("already have this item!");
      g_mutex_unlock(&ctx->m);
      return FALSE;
    }
  }

  gdouble left, top, right, bottom;
  goo_canvas_get_bounds (GOO_CANVAS(priv->canvas), &left, &top, &right, &bottom);
  gdouble rx, ry;

  rx = x * PROSCENION_PIX_PER_METER + (right - left) / 2;
  ry = y * PROSCENION_PIX_PER_METER + (bottom - top) / 2;
  GooCanvasItem *root = goo_canvas_get_root_item (GOO_CANVAS(priv->canvas));
  GooCanvasItem *group = goo_canvas_group_new (root, "x", rx, "y", ry, NULL);
  GooCanvasItem *circle = goo_canvas_ellipse_new (group, 0.0, 0.0, 5.0, 5.0, NULL);
  GooCanvasItem *label = goo_canvas_text_new (group, name, 0.0, 0.0 - 10.0, 0, 0, "font", "Sans 8", "fill-color", capture ? "blue" : "black", NULL);

  ProscenionItem *pitem = proscenion_item_new(name, x, y, sens, capture, group);
  g_info ("registering jack ports for %s\n", name);
  if (capture) {
    gchar *lname = g_strdup_printf ("%s-L", pitem->name);
    pitem->portL = jack_port_register (ctx->jack->client, lname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    g_free(lname);
    gchar *rname = g_strdup_printf ("%s-R", pitem->name);
    pitem->portR = jack_port_register (ctx->jack->client, rname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    g_free(rname);
  } else {
    gchar *lname = g_strdup_printf ("%s-L", pitem->name);
    pitem->portL = jack_port_register (ctx->jack->client, lname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    g_free(lname);
    gchar *rname = g_strdup_printf ("%s-R", pitem->name);
    pitem->portR = jack_port_register (ctx->jack->client, rname, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    g_free(rname);
  }

  if (!pitem->portL || !pitem->portR) {
    g_warning ("no more JACK ports available\n");
    pitem->registered = FALSE;
  } else {
    pitem->registered = TRUE;
  }

  ctx->items = g_list_append(ctx->items, pitem);
  if (capture)
    ctx->capture = pitem;

  g_signal_connect (group, "button-press-event", G_CALLBACK(on_item_button_press), win);
  g_signal_connect (group, "button-release-event", G_CALLBACK(on_item_button_press), win);
  g_signal_connect (group, "motion-notify-event", G_CALLBACK(on_item_motion), win);

  g_mutex_unlock(&ctx->m);
  return TRUE;
}
