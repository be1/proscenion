#include <math.h>
#include <stdio.h>
#include "proscenion_shared_ctx.h"
#include "proscenion_jack.h"
#include "proscenion_item.h"

#define AIRSPEED 340.0 	/* sound speed in meter per seconds */
#define CAPRADIUS 0.1   /* headset radius in meter */

ProscenionJack *psc_jack_new()
{
	jack_status_t status;
	ProscenionJack *jack = calloc(sizeof(ProscenionJack), 1);
	jack->client = jack_client_open(PSC_JACKNAME, JackNullOption, &status, NULL);
	if (!jack->client) {
		fprintf (stderr, "jack_client_open() failed, "
				"status = 0x%2.0x\n", status);
		if (status & JackServerFailed) {
			fprintf (stderr, "Unable to connect to JACK server\n");
		}

		free (jack);
		return NULL;
	}

	if (status & JackServerStarted) {
		fprintf (stderr, "JACK server started\n");
	}

	if (status & JackNameNotUnique) {
		jack->name = jack_get_client_name(jack->client);
		fprintf (stderr, "unique name `%s' assigned\n", jack->name);
	} else {
		jack->name = PSC_JACKNAME;
	}

	jack->activated = FALSE;
	return jack;
}

void psc_jack_start (ProscenionJack *jack)
{
	if (jack->activated) {
		fprintf (stderr, "jack already started");
		return;
	}

	if (jack_activate(jack->client)) {
		fprintf (stderr, "cannot start jack client");
	} else {
		jack->activated = TRUE;
	}
}

void psc_jack_destroy(ProscenionJack *jack)
{
	jack_client_close (jack->client);
	jack->activated = FALSE;
}

int psc_jack_process (jack_nframes_t nframes, void *arg)
{
	ProscenionSharedCtx *ctx = arg;
	int rate = jack_get_sample_rate(ctx->jack->client);

	g_mutex_lock(&ctx->m);
  ProscenionItem *capitem = ctx->capture;

	if (capitem) {
		jack_default_audio_sample_t *outL, *outR;

		const float capX = capitem->x;
		const float capY = capitem->y;
		const gdouble capSens = capitem->sens;

		outL = jack_port_get_buffer (capitem->portL, nframes);
		memset (outL, 0, sizeof (jack_default_audio_sample_t) * nframes);
		outR = jack_port_get_buffer (capitem->portR, nframes);
		memset (outR, 0, sizeof (jack_default_audio_sample_t) * nframes);

		GList *it;
		for (it = ctx->items; it; it = it->next) {
			ProscenionItem *pitem = it->data;
			if (pitem->capture) {
				continue;
			}

			const float x = pitem->x;
			const float y = pitem->y;

			float l_tmp = pow(x-capX+CAPRADIUS, 2.0) + pow(y-capY, 2.0);
			float r_tmp = pow(x-capX-CAPRADIUS, 2.0) + pow(y-capY, 2.0);

			float l_att = capSens * pitem->sens / l_tmp;
			float l_dly = sqrt(l_tmp) / AIRSPEED;

			float r_att = capSens * pitem->sens / r_tmp;
			float r_dly = sqrt(r_tmp) / AIRSPEED;

			int l_pos = l_dly * rate;
			int r_pos = r_dly * rate;

			jack_default_audio_sample_t *inL, *inR;

			inL = jack_port_get_buffer (pitem->portL, nframes);
			inR = jack_port_get_buffer (pitem->portR, nframes);

			for (int i = 0; i < nframes; i++) {
				int l_idx = (pitem->ring_beg + l_pos) % pitem->ring_len;
				int r_idx = (pitem->ring_beg + r_pos) % pitem->ring_len;

				pitem->ringL[l_idx] = inL[i] * l_att;
				pitem->ringR[r_idx] = inR[i] * r_att;

				outL[i] += pitem->ringL[pitem->ring_beg];
				outR[i] += pitem->ringR[pitem->ring_beg];

				pitem->ring_beg = (pitem->ring_beg + 1) % pitem->ring_len;
			}
		}
	}

	g_mutex_unlock(&ctx->m);

	return 0;
}

void psc_jack_shutdown (void *arg)
{
	ProscenionSharedCtx *ctx = arg;
}

