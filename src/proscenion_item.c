#include "proscenion_item.h"
#include "proscenion_jack.h"

ProscenionItem *proscenion_item_new(const gchar *name, gdouble x, gdouble y, gdouble sens, gboolean capture, GooCanvasItem *item)
{
	ProscenionItem *pitem = malloc (sizeof (ProscenionItem));
	memset(pitem, 0, sizeof(ProscenionItem));

	pitem->name = g_strdup(name);
	pitem->item = item;
	pitem->x = x;
	pitem->y = y;
	pitem->sens = sens; /* sensitivity */
	pitem->grabbed = FALSE;
	pitem->registered = FALSE;
	pitem->capture = capture;
	pitem->portL = NULL;
	pitem->portR = NULL;
	pitem->ring_len = 9600 * sizeof(jack_default_audio_sample_t);
	pitem->ring_beg = 0;
	pitem->ringL = calloc (pitem->ring_len, sizeof (jack_default_audio_sample_t));
	pitem->ringR = calloc (pitem->ring_len, sizeof (jack_default_audio_sample_t));
	return pitem;
}
