#ifndef __PROSCENION_JACK_H
#define __PROSCENION_JACK_H

#include <glib.h>
#include <jack/jack.h>

#define PSC_JACKNAME "proscenion"
#define PSC_JACK_VALUE_SIZE sizeof(float)
#define PSC_JACK_RING_SIZE (64 * 1024 * PSC_JACK_VALUE_SIZE)

typedef struct _ProscenionJack {
	jack_client_t *client;
	gboolean activated;
	char *name;
} ProscenionJack;

ProscenionJack *psc_jack_new ();
void psc_jack_start (ProscenionJack *jack);
void psc_jack_destroy (ProscenionJack *jack);
int psc_jack_process (jack_nframes_t nframes, void *arg);
void psc_jack_shutdown (void *arg);

#endif
