#include <glib.h>
#include <gtk/gtk.h>

#include <libintl.h>
#include <locale.h>

#define  _(x)     gettext (x)
#define N_(x)     x
#define C_(ctx,x) pgettext (ctx, x)

#include "proscenion_app.h"
#include "proscenion_app_win.h"
#include "menu_callbacks.h"

struct _ProscenionApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE(ProscenionApp, proscenion_app, GTK_TYPE_APPLICATION);

static void
proscenion_app_init (ProscenionApp *app)
{
  GSimpleAction *new_action;
  GSimpleAction *open_action;
  GSimpleAction *save_action;
  GSimpleAction *addsrc_action;
  /* GSimpleAction *addcap_action; */
  GSimpleAction *about_action;

  /* actions */
  new_action = g_simple_action_new("new", NULL);
  g_signal_connect(new_action, "activate", G_CALLBACK (new_callback), app);
  g_action_map_add_action(G_ACTION_MAP (app), G_ACTION (new_action));

  open_action = g_simple_action_new("open", NULL);
  g_signal_connect(open_action, "activate", G_CALLBACK (open_callback), app);
  g_action_map_add_action(G_ACTION_MAP (app), G_ACTION (open_action));
 
  save_action = g_simple_action_new("save", NULL);
  g_signal_connect(save_action, "activate", G_CALLBACK (save_callback), app);
  g_action_map_add_action(G_ACTION_MAP (app), G_ACTION (save_action));

  addsrc_action = g_simple_action_new("add-source", NULL);
  g_signal_connect(addsrc_action, "activate", G_CALLBACK (addsrc_callback), app);
  g_action_map_add_action(G_ACTION_MAP (app), G_ACTION (addsrc_action));

  about_action = g_simple_action_new("about", NULL);
  g_signal_connect(about_action, "activate", G_CALLBACK (about_callback), app);
  g_action_map_add_action(G_ACTION_MAP (app), G_ACTION (about_action));
}

static void
proscenion_app_activate (GApplication *app)
{
  /* menus */
  GMenu *menu, *stage_menu, *item_menu, *help_menu;

  menu = g_menu_new();

  stage_menu = g_menu_new();
  g_menu_append (stage_menu, _("New..."), "app.new");
  g_menu_append (stage_menu, _("Open..."), "app.open");
  g_menu_append (stage_menu, _("Save..."), "app.save");
  g_menu_append_submenu (menu, _("Stage"), G_MENU_MODEL (stage_menu));

  item_menu = g_menu_new();
  g_menu_append (item_menu, _("Add source..."), "app.add-source");
  /* g_menu_append (item_menu, _("Add capture..."), "app.add-capture"); */
  g_menu_append_submenu (menu, _("Items"), G_MENU_MODEL (item_menu));

  help_menu = g_menu_new();
  g_menu_append (help_menu, _("About..."), "app.about");
  g_menu_append_submenu (menu, _("Help"), G_MENU_MODEL (help_menu));
  gtk_application_set_menubar (GTK_APPLICATION (app), G_MENU_MODEL(menu));

  ProscenionAppWindow *win;
  win = proscenion_app_window_new (PROSCENION_APP (app));
  gtk_window_present (GTK_WINDOW (win));

}

static void
proscenion_app_open (GApplication  *app,
                  GFile        **files,
                  gint           n_files,
                  const gchar   *hint)
{
  GList *windows;
  ProscenionAppWindow *win;
  int i;

  for (i = 0; i < n_files; i++) {
    win = proscenion_app_window_new (PROSCENION_APP (app));
    proscenion_app_window_open (win, files[i]);
    gtk_window_present (GTK_WINDOW (win));
  }
}

static void
proscenion_app_class_init (ProscenionAppClass *class)
{
  G_APPLICATION_CLASS (class)->activate = proscenion_app_activate;
  G_APPLICATION_CLASS (class)->open = proscenion_app_open;
}

ProscenionApp *
proscenion_app_new (void)
{
  return g_object_new (PROSCENION_APP_TYPE,
                       "application-id", "com.servebeer.herewe.Proscenion",
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       NULL);
}
