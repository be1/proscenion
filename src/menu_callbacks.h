#ifndef _MENU_CALLBACKS_H
#define _MENU_CALLBACKS_H

#include <glib.h>
#include <gtk/gtk.h>

#define PROSCENION_FILE_PATTERN "*.psc"

void
new_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data);

void
open_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data);

void
save_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data);

void
run_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data);

void
addsrc_callback (GSimpleAction *simple,
         GVariant      *parameter,
         gpointer       user_data);

void
about_callback (GSimpleAction *simple,
            GVariant      *parameter,
            gpointer       user_data);
#endif
