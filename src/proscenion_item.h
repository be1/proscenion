#ifndef __PROSCENION_ITEM_H
#define __PROSCENION_ITEM_H

#include <glib.h>
#include <gtk/gtk.h>
#include <goocanvas.h>
#include <jack/jack.h>
#include <jack/ringbuffer.h>

typedef struct _ProscenionItem {
	gchar *name;
	GooCanvasItem *item;
	gdouble x, y, sens;
	gboolean grabbed, capture, registered;
	jack_port_t *portL;
	jack_port_t *portR;

	jack_default_audio_sample_t *ringL;
	jack_default_audio_sample_t *ringR;
	int ring_len;
	int ring_beg;
} ProscenionItem;

ProscenionItem *proscenion_item_new(const gchar *name, gdouble x, gdouble y, gdouble sens, gboolean capture, GooCanvasItem *item);

#endif
