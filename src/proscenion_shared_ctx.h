#ifndef __PROSCENION_SHARED_CTX_H
#define __PROSCENION_SHARED_CTX_H

#include <glib.h>
#include "proscenion_jack.h"
#include "proscenion_item.h"

typedef struct _ProscenionSharedCtx {
  ProscenionJack *jack;
  GList *items;
  ProscenionItem *capture;
  GMutex m;
} ProscenionSharedCtx;

ProscenionSharedCtx *proscenion_shared_ctx_new ();
#endif
