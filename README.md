# proscenion

An audio open stage simulation tool written in C with Gtk and Jack.

prerequisites:
- intltoolize
- autoconf
- autoconf-archive
- automake
- libtool
- gobject-introspection
- libgoocanvas-2.0-dev
- libgtk-3-dev
- libxml2-dev
- libjack-jackd2-dev

run:
- ./autogen.sh
- make && sudo make install
- proscenion
